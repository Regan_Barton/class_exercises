﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assessment_comp6211
{
    class Program
    {
        static void Main(string[] args)
        {
            //Question 1 Enqueue.
            Queue MyQueue = new Queue();
            MyQueue.Enqueue("one");
            MyQueue.Enqueue("two");
            MyQueue.Enqueue("three");
            MyQueue.Enqueue("four");
            MyQueue.Enqueue("five");

            //Question 1 Copy to array.
            object[] queuearray = MyQueue.ToArray();

            Console.WriteLine("Array Contains:");
            foreach (var item in queuearray)
            {
                Console.Write("{0} ", item);
            }

            Console.Write("\n\nMyQueue contains:");
            printQueue(MyQueue);


            Console.WriteLine("\nSearch Queue for number: "); //Searches the queue.
            var search = Console.ReadLine();
            Console.WriteLine("\nMyQueue.Contains: = {0}", MyQueue.Contains(search));



            Console.WriteLine("\nDo you want to remove an item from the queue?"); //Queue Dequeue.
            string answer = Console.ReadLine();
            if (answer == "yes")
            {
                MyQueue.Dequeue();
            }


            Console.Write("\nMyQueue contains:");
            printQueue(MyQueue);

            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }


        public static void printQueue(IEnumerable myCollection)
        {
            foreach (Object obj in myCollection)
                Console.Write("{0} ", obj);
            Console.WriteLine();
        }
    }
}

