﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting_algorithms
{
    class Program
    {
        static void Main(string[] args)
    {

            Console.WriteLine("\nBubble Sorting Algorithm: ");
            Bubblesort();//Calls the bubble sort algorithm.

            Console.WriteLine("\nSelection Sorting Algorithm: ");
            Selectionsort();//Calls the selection sort algorithm.

            Console.WriteLine("\nInsert Sorting Algorithm");
            Insertionsort();

            Console.ReadLine();
        }

        public static void Bubblesort()
        {
            int[] bb = { 48, 32, 56, 9, 22, 3 };//Array to be sorted
            int tt = 0;


            for (int write = 0; write < bb.Length; write++) //Block of code that does the sorting.
            {
                for (int sort = 0; sort < bb.Length - 1; sort++)
                {
                    if (bb[sort] > bb[sort + 1])
                    {
                        tt = bb[sort + 1];
                        bb[sort + 1] = bb[sort];
                        bb[sort] = tt;
                    }
                }
                Displaybubble(bb); //displays the method that prints the sorting to the console.
            }
        }

        public static void Selectionsort()
        {
            int[] ss = { 48, 32, 56, 9, 22, 3 };//Array to be sorted
            int ts, min;

            for (int i = 0; i < ss.Length - 1; i++) //Block of code sorts the array.
            {
                min = i;
                for (int j = i + 1; j < ss.Length; j++)
                {
                    if (ss[j] < ss[min])
                    {
                        min = j;
                    }
                }
                if (min != 1)
                {
                    ts = ss[i];
                    ss[i] = ss[min];
                    ss[min] = ts;
                }
                Displaysort(ss);
            }
        }

        public static void Insertionsort()
        {
            int[] ii = { 48, 32, 56, 9, 22, 3 };//Array to be sorted
            int ti, t;


            for (int i = 1; i <= ii.Length - 1; i++)
            {
                ti = i;
                t = ii[i];
                while (ti > 0 && ii[ti - 1] >= t)
                {
                    ii[ti] = ii[ti - 1];
                    ti -= 1;
                }
                ii[ti] = t;
                Displayinsert(ii);
            }


        }

        public static void Displaybubble(int[] _bb) //Block of code prints the whole process in steps to the console.
        {
            for (int i = 0; i <= _bb.Length - 1; i++)
            {
                Console.Write(_bb[i] + " ");
            }
            Console.WriteLine();
        }

        public static void Displaysort(int[] _ss) //Block of code prints the whole process in steps to the console.
        {
            for (int i = 0; i <= _ss.Length - 1; i++)
            {
                Console.Write(_ss[i] + " ");
            }
            Console.WriteLine();
        }

        public static void Displayinsert(int[] _ii) //Block of code prints the whole process in steps to the console.
        {
            for (int i = 0; i <= _ii.Length - 1; i++)//for loop used to display the contents of an array
            {
                Console.Write(_ii[i] + " ");
            }
            Console.WriteLine();
        }
    }
}
