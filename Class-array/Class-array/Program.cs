﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class_array
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] id = new int[10] {00, 11, 22, 33, 44, 55, 66, 77, 88, 99};
            
            string[] names = new string[10] { "Jeremy", "Regan", "Andrew", "Johnny", "Mark", "Sam", "Tony", "Steven", "Jared", "Kainen", };


            //Get Length of Array
            Console.WriteLine("1.Get Length of Array");
            Console.WriteLine();
            int l;
            l = id.Length;
            Console.WriteLine(l);
            Console.ReadKey();
            Console.Clear();

            //Copy array into new one
            Console.WriteLine("2.Copy Array into new one");
            Console.WriteLine();
            int[] copied = new int[10];
            Array.Copy (id, copied, 10);
            Console.WriteLine(copied[5]);
            Console.ReadKey();
            Console.Clear();

            //Get Array Type
            Console.WriteLine("3.Get Array type");
            Console.WriteLine();
            Type type = id.GetType().GetElementType();
            Console.WriteLine(type);
            Console.ReadKey();
            Console.Clear();

            //Get Value of Array at location 5
            Console.WriteLine("4.Get Value of Array at location 5");
            Console.WriteLine();
            Console.WriteLine(id[4]);
            Console.ReadKey();
            Console.Clear();

            //Search for array value and display output
            Console.WriteLine("5.Search for array value and display output");
            Console.WriteLine();
            Console.WriteLine("What value of the array id do you want to search True if it is in array false if it isnt");
            int x = Convert.ToInt32(Console.ReadLine());
            int t = Array.IndexOf(id, x);
            if (t > -1)
            {
                Console.WriteLine("True");
            }
            else
            {
                Console.WriteLine("False");
            }
            Console.ReadKey();
            Console.Clear();



            //Reverse Array
            Console.WriteLine("6.Reverse Array");
            Console.WriteLine();
            Array.Reverse(id);
            foreach (var item in id)
            {
                Console.WriteLine(item.ToString());
            }
            //reversing it back to original
            Array.Reverse(id);
            Console.ReadKey();
            Console.Clear();

            //Change value of both arrays at location 5
            Console.WriteLine("7.Change value of both arrays at location 5");
            Console.WriteLine();
            Console.WriteLine("What Number Do You Want To Replace id[5] With?");
            int i = Convert.ToInt32(Console.ReadLine());
            id[4] = i;
            Console.WriteLine(id[4]);
            Console.ReadKey();

            Console.WriteLine("What Number Do You Want To Replace names[5] With?");
            var n = Convert.ToString(Console.ReadLine());
            names[4] = n;
            Console.WriteLine(names[4]);
            Console.ReadKey();
            Console.Clear();

            //Sort arrays in ascending and descending orders
            Console.WriteLine("8.Sort Arrays in ascending and descending order");
            Console.WriteLine();
            Array.Sort(id);
            foreach (var item in id)
            {
                Console.WriteLine(item.ToString());
            }
            Console.ReadKey();
            Array.Reverse(id);
            foreach (var item in id)
            {
                Console.WriteLine(item.ToString());
            }
            Console.ReadKey();
        }
    }
}
