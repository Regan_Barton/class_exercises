﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circle
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is the radius of the circle?");
            double e = Convert.ToDouble(Console.ReadLine());
            double f = Math.PI;
            Console.WriteLine("The Perimeter of the circle is:"); Console.WriteLine((2 * f) * e);
            Console.WriteLine("The Area of the circle is:"); Console.WriteLine((e * e) * f);
            Console.ReadKey();
        }
    }
}
