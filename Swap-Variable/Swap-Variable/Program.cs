﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Swap_Variable
{
    class Program
    {
        static void Main(string[] args)
        {
            int g = 5;
            int h = 7;
            int i;
            Console.WriteLine("Variable g is"); Console.WriteLine(g); Console.WriteLine("Variable h is"); Console.WriteLine(h);
            Console.ReadLine();
            i = h;
            h = g;
            g = i;

            Console.WriteLine("Variable g is"); Console.WriteLine(g); Console.WriteLine("Variable h is"); Console.WriteLine(h);
            Console.ReadLine();
        }
    }
}
