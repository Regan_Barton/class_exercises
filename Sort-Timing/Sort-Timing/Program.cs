﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sort_Timing
{
    class Program
    {
                class CArray
        {
            private int[] arr;
            private int upper;
            private int numElements;
            public CArray (int size)
            {
                arr = new int[size];
                upper = size - 1;
                numElements = 0;
            }
            public void Insert(int item)
            {
                arr[numElements] = item;
                numElements++;
            }
            public void DisplayElements()
            {
                for (int i = 0; i <= upper; i++)
                    Console.Write(arr[i] + " ");
            }
            public void Clear()
            {
                for (int i = 0; i <= upper; i++)
                    arr[i] = 0;
                numElements = 0;
            }
        }
        static void Main(int args)
        {
            CArray nums = new CArray(args);
            for (int i = 0; i <= 49; i++)
                nums.Insert(i);
            nums.DisplayElements();


            Timing sortTime = new Timing();
            Random rnd = new Random(100);
            int numItems = 1000;
            CArray theArray = new CArray(numItems);
            for (int i = 0; i < numItems; i++)
            {
                theArray.Insert((int)(rnd.NextDouble() * 1000));
                sortTime.startTime();
                theArray.SelectionSort();
                sortTime.stopTime();
                Console.WriteLine("Selection Sort: " + sortTime.getResult().TotalMilliseconds);
                theArray.Clear();
            }

            for (int i = 0; i < numItems; i++)
            {
                theArray.Insert((int)(rnd.NextDouble() * 100));
                sortTime.startTime();
                theArray.BubbleSort();
                sortTime.stopTime();
                Console.WriteLine("Bubble sort: " + sortTime.getResult().TotalMilliseconds);
                theArray.Clear();
            }
            for (int i = 0; i < numItems; i++)
            {
                theArray.Insert((int)(rnd.NextDouble() * 100));
                sortTime.startTime();
                theArray.InsertionSort();
                sortTime.stopTime();
                Console.WriteLine("Insertion sort: " + sortTime.getResult().TotalMilliseconds);
                theArray.Clear();
            }
        }
    }
}
