﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Highest_number
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is First number");
            int k = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("What is Second number");
            int l = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("What is Third number");
            int m = Convert.ToInt32(Console.ReadLine());

            var n = new int[] { k, l, m };

            Array.Sort(n);

            int lowestNumber = n[0];
            int middle = n[1];
            int highest = n[2];

            Console.WriteLine("The highest number is"); Console.WriteLine(highest);

            Console.ReadLine();
        }
    }
}
