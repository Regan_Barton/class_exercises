﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue_Exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            //Add items to queue
            Queue qu = new Queue();
            Console.WriteLine("1. Insert Words into Queue");
            qu.Enqueue(Console.ReadLine());


            //remove items from queue
            Queue coo = new Queue();
            Console.WriteLine("\n2. Insert Words into Queue");
            coo.Enqueue(Console.ReadLine());

            coo.Clear();

            //show number of items in queue
            Queue q = new Queue();
            Console.WriteLine("\n3. Count Words in the Queue");
            q.Enqueue("one");
            q.Enqueue("two");
            q.Enqueue("three");
            q.Enqueue("four");
            q.Enqueue("five");

            Console.WriteLine(q.Count);
            Console.ReadLine();  

            //Find an item in queue
            Queue qq = new Queue();
            Console.WriteLine("\n4. Find an item in queue");
            qq.Enqueue("one");
            qq.Enqueue("two");
            qq.Enqueue("three");
            qq.Enqueue("four");
            qq.Enqueue("five");

            Console.WriteLine("\nSearch Queue Numbers 1-5");
            var search = Console.ReadLine();


            Console.WriteLine("\nqq.Contains: = {0}",
                qq.Contains(search));
            //
            Console.ReadLine();

            //Display Items in Queue
            Queue qqq = new Queue();
            Console.WriteLine("\n5. Display Items in Queue.");
            qqq.Enqueue('1');
            qqq.Enqueue('2');
            qqq.Enqueue('3');
            qqq.Enqueue('4');
            qqq.Enqueue('5');

            Console.WriteLine("\nCurrent queue: ");
            foreach (char c in qqq)
            {

                Console.Write(c + " ");
                Console.WriteLine();
            }
            Console.ReadLine();  



        }
    }
}
