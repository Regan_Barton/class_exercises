﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multiplication_Table
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What Number for multiplication table");
            int c = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Table is:");
            int d = 0;
            while (d <= 10)
            {
                Console.WriteLine(c * d);
                d++;
            }
            Console.ReadKey();
        }
    }
}
