﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Stack_exercises
{
    class Program
    {
        static void Main(string[] args)
        {
            //Question 2 Checking Palindrome.
            Stack<char> palindrome = new Stack<char>();
            Console.WriteLine("Enter string to determine if it is a palindrome:");
            string input = Console.ReadLine() ;
            input = Regex.Replace(input, @"[^\w]", string.Empty); //This code removes spaces and punctuation

            var inputToUpper = input.ToUpper(); //This makes the string all uppercase which technically is ignoring capitalization.

            foreach (char c in inputToUpper)
            {
                palindrome.Push(c);
            }

            //Checking string if it is a palindrome.
            bool isPalindrome = true;
            var noOfItems = palindrome.Count;

            for (int i = 0; i < noOfItems; i++)
            {
                if (inputToUpper[i] != palindrome.Pop())
                {
                    isPalindrome = false; break; //
                }
            }

            //Displays whether or not code is a palindrome.
            if (isPalindrome)
            {
                Console.WriteLine("String is a Palindrome");
            }
            else
            {
                Console.WriteLine("String is not a Palindrome");
            }
            //Code to exit program
            Console.WriteLine("\nPress any key to exit:");
            Console.ReadKey();
        }


    }
    }

